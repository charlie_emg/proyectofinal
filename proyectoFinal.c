//Carlos Ayala Vega A01337893
//Carlos Eduardo Mu�oz Gonzalez A01332264
#include <stdio.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <string.h>
#include <arpa/inet.h>
#include <stdlib.h>
#include <syslog.h>
#include <sys/stat.h>
#include <unistd.h>

#define SIZE 8
#define MSGSIZE 1024

char LosIp[20][64];
char puertos[20][64];
int puerto;
int length;
int perte;

void ipAdd(int s, char buffer[MSGSIZE]){
  FILE *sout = fdopen(s, "w");
  fprintf(sout, "HTTP/1.0 200 OK\r\n");
  fprintf(sout, "Content-Type: text/plain\r\n");
  fprintf(sout, "Content-Length: %d\r\n", perte);
  fprintf(sout, "\r\n");
  for (int i = 0; i < length; i++) {
     fprintf(sout, "%s\n", LosIp[i]);
     printf("%s\n", LosIp[i]);
  }
  fflush(0);
}

void agregarIP(char *message){
  char *Recit = strtok(message, ":");
  while (Recit != NULL){
    strcpy(LosIp[length], Recit);
    printf("added ip to LosIp[%d]: %s\n", length, LosIp[length]);
    Recit = strtok(NULL, ":");
    strcpy(puertos[length], Recit);
    Recit = strtok(NULL, ":");
    length++;
    for (int i = 0; i < length; i++){
      printf("ip[%d]: %s\n", i, LosIp[i]);
    }
  }
}
void msg(char *message, char *ip, char *puerto){
  int sd, size;
  struct addrinfo hints, *res;
  char buffer[MSGSIZE];
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  NInf(ip, puerto, &hints, &res);
  int luf = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
  connect(luf, res->ai_addr, res->ai_addrlen);
  sprintf(buffer, "GET /send/%s HTTP/1.1 OK\r\n", message);
  write(luf, buffer, strlen(buffer));
  fflush(0);
  close(luf);
  freeaddrinfo(res);
  sleep(1);
}

void eP(char *my_ip, char *ip, char *puerto){
  int size;
  struct addrinfo hints, *res;
  char buffer[MSGSIZE];
  memset(&hints, 0, sizeof(struct addrinfo));
  hints.ai_family = AF_INET;
  hints.ai_socktype = SOCK_STREAM;
  NInf(ip, puerto, &hints, &res);
  int luf = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
  connect(luf, res->ai_addr, res->ai_addrlen);
  
  sprintf(buffer, "GET /peer/%s:%d HTTP/1.1 OK\r\n", my_ip, puerto);
  write(luf, buffer, strlen(buffer));
  fflush(0);
  close(luf);
  freeaddrinfo(res);
  sleep(1);
}
void recibidos(char *request, char *message, int s, char buffer[MSGSIZE]){
   if (strcmp(request, "list") == 0){
    ipAdd(s, buffer);
  }
  else{
    if (strcmp(request, "send") == 0)  {     
      for (int i = 0; i < length; i++) {
        msg(message, LosIp[i], puertos[i]);
      }
    }
    else{
      if (strcmp(request, "peer") == 0) {
        agregarIP(message);
      }
      else{
         printf("Invalid Command\n");
      }
    }
  }
}

void serve(int s){
  char buffer[MSGSIZE];
  int i = 0;
  char *Recit;
  char Recits[MSGSIZE][MSGSIZE];
  FILE *sin = fdopen(s, "r");
  while (fgets(buffer, MSGSIZE, sin) != NULL) {
    printf("%s\n", buffer);
    Recit = strtok(buffer, " /");
    while (Recit != NULL){
      strcpy(Recits[i], Recit);
      Recit = strtok(NULL, " /");
      i++;
    }
    if (buffer[0] == '\r' && buffer[1] == '\n'){
      break;
    }
  }
  sleep(1);
  recibidos(Recits[1], Recits[2], s, buffer);
}

void empezarC(char *ip, char *puerto){
  char buffer[MSGSIZE];
  int clientSocket, ret, res;
  struct sockaddr_in serverAddr;
  struct sockaddr_in addr;
  socklen_t addr_SIZE = sizeof(addr);
  char *my_ip;
  clientSocket = socket(AF_INET, SOCK_STREAM, 0);

  if (clientSocket < 0){
    printf("Error in connection\n");
    exit(1);
  }
  memset(&serverAddr, '\0', sizeof(serverAddr));
  serverAddr.sin_family = AF_INET;
  serverAddr.sin_port = htons(atoi(puerto));
  serverAddr.sin_addr.s_addr = inet_addr(ip);
  ret = connect(clientSocket, (struct sockaddr *)&serverAddr, sizeof(serverAddr));
  res = getsockname(clientSocket, (struct sockaddr *)&addr, &addr_SIZE);
  if (ret < 0){
    printf("Error in connection\n");
    exit(1);
  }
  my_ip = inet_ntoa(addr.sin_addr);
  eP(my_ip, ip, puerto);
  printf("Connected to: %s:%s\n", ip, puerto);
}

void initiate() {
	int miNom, r;
	struct sockaddr_in sin, pin;
	socklen_t addrlen;
	char *buffer;
	int i = 0;
	for (i = 0; i < length; i++) {
		empezarC(LosIp[i], puertos[i]);
	}
	int socketN = socket(AF_INET, SOCK_STREAM, 0);
	memset(&sin, 0, sizeof(sin));
	sin.sin_family = AF_INET;
	sin.sin_addr.s_addr = INADDR_ANY;
	sin.sin_port = htons(puerto);
	r = bind(socketN, (struct sockaddr *)&sin, sizeof(sin));
	if (r < 0) {
		perror("bind");
	}
	listen(socketN, 5);
	addrlen = sizeof(pin);
	while ((miNom = accept(socketN, (struct sockaddr *)&pin, &addrlen)) > 0) {
		if (!fork()) {
			printf("Connected from: %s\n", inet_ntoa(pin.sin_addr));
			printf("Port: %d\n", ntohs(pin.sin_port));
			serve(miNom);
			close(miNom);
			exit(0);
		}
	}
	close(socketN);
	sleep(1);
}

int main(int argc, char *argv[]) {
	int j = 0;
	char *Recit;
	length = argc - 2;
	puerto = (atoi)(argv[1]);
	for (int i = 2; i < argc; ++i) {
		Recit = strtok(argv[i], ":");
		while (Recit != NULL) {
			strcpy(LosIp[j], Recit);
			Recit = strtok(NULL, ":");
			strcpy(puertos[j], Recit);
			Recit = strtok(NULL, ":");
			j++;
		}
	}
	perte = (strlen(LosIp[0])) * length;
	initiate();
	return 0;
}